# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# # Teilchenraten den Teilchen zuordnen:

# %% [markdown]
#Importieren von pandas und pyplot
import pandas as pd
import matplotlib.pyplot as plt

# %% [markdown]
# Myonen haben einen Teilchenfluss von ca. 100 pro Quadratmeter und Sekunde. Wieviel sind es pro Minute?

# %%
M=100*60
print(M, 'pro Quadratmeter und Minute')

# %% [markdown]
# #Gamma haben einen Teilchenfluss von ca. 0.04 - 0.964 pro Quadratzentimeter und Minute (Je nach Energie). Wieviel sind es pro Quadratmeter?

# %%
G1= 0.04*1000
G2= 0.964*1000
print(G1, '-', G2, 'pro Quadratmeter und Minute')


# %%
#Importieren der Daten als np array
A110 = pd.read_csv('A110.csv', parse_dates=["Date/Time"])
A101 = pd.read_csv('A101.csv', parse_dates=["Date/Time"])
A011 = pd.read_csv('A011.csv', parse_dates=["Date/Time"])

#Umbenennung der Spalten zur besseren Handhabung
A011 = A011.rename(
    columns={
        " SEVAN Aragats  Coincidence 011": "011"
    }
)
A101 = A101.rename(
    columns={
        " SEVAN Aragats  Coincidence 101": "101"
    }
)
A110 = A110.rename(
    columns={
        " SEVAN Aragats  Coincidence 110": "110"
    }
)


# %%
#Tabelle ausgeben
print(A110)
print(len(A110))



# %%
#Plotten der Daten
plt.plot(A110["110"], label='110')
plt.plot(A101["101"], label='101')
plt.plot(A011["011"], label='011')
plt.legend()
plt.xlabel('Time in Minutes')
plt.ylabel('Count rate per Minute')
plt.title('Aragats 10.-11.02.21')


# %%
#Plotten der Datenverteilung als Histogramm
plt.hist(A110["110"], bins=100, label='110')
plt.hist(A101["101"], bins=100, label='101')
plt.hist(A011["011"], bins=100, label='011')
plt.legend()
plt.xlabel('Count Rate')
plt.ylabel('Events')
plt.title('Aragats 10.-11.02.21')


# %%
#Mittelwert und Standartabweichung berechnen und schauen wo es im Histogramm liegt
mean110=A110["110"].mean()
std110=A110["110"].std()

plt.hist(A110["110"], bins=100, label='110')
plt.axvline(x=mean110, color='black', linestyle='dashed', label='Mittelwert')
plt.axvline(x=mean110-std110, color='grey', linestyle='dashed', label='Standartabweichung')
plt.axvline(x=mean110+std110, color='grey', linestyle='dashed')
plt.legend()
plt.xlabel('Count Rate')
plt.ylabel('Events')
plt.title('Aragats 10.-11.02.21')


# %%
mean101=A101["101"].mean()
std101=A101["101"].std()

plt.hist(A101["101"], bins=100, label='101', color='orange')
plt.axvline(x=mean101, color='black', linestyle='dashed', label='Mittelwert')
plt.axvline(x=mean101-std101, color='grey', linestyle='dashed', label='Standartabweichung')
plt.axvline(x=mean101+std101, color='grey', linestyle='dashed')
plt.legend()
plt.xlabel('Count Rate')
plt.ylabel('Events')
plt.title('Aragats 10.-11.02.21')


# %%
mean011=A011["011"].mean()
std011=A011["011"].std()

plt.hist(A011["011"], bins=100, label='011', color='green')
plt.axvline(x=mean011, color='black', linestyle='dashed', label='Mittelwert')
plt.axvline(x=mean011-std011, color='grey', linestyle='dashed', label='Standartabweichung')
plt.axvline(x=mean011+std011, color='grey', linestyle='dashed')
plt.legend()
plt.xlabel('Count Rate')
plt.ylabel('Events')
plt.title('Aragats 10.-11.02.21')


# %%
#Alle Mittelwerte:
print('Rate für 110:', round(mean110,2), '+/-', round(std110,2))
print('Rate für 101:', round(mean101,2), '+/-', round(std101,2))
print('Rate für 011:', round(mean011,2), '+/-', round(std011,2))

# %% 
# # Wo werden Myonen gemessen? (Vergleich mit Literaturwert)
# -> 101
# %% 
# # Wo werden Gammas gemessen? (Vergleich mit Literaturwert)
# -> 011
# %% [markdown]
# # Verschiedene Höhen:
# 

# %%
Berlin = pd.read_csv('Berlin.csv',sep=",",header=0, names=["Date/Time","Berlin"], parse_dates=["Date/Time"])
Aragats = pd.read_csv('Aragats.csv',sep=",",header=0, names=["Date/Time","Aragats"], parse_dates=["Date/Time"])
Prag = pd.read_csv('Prague.csv',sep=",",header=0, names=["Date/Time","Prag"], parse_dates=["Date/Time"])


# %%
print(Berlin)
print(len(Berlin))

# %%
plt.plot(Berlin["Berlin"], label='Berlin')
plt.plot(Aragats["Aragats"], label='Aragats')
plt.plot(Prag["Prag"], label='Prag')
plt.legend()
plt.xlabel('Time in Minutes')
plt.ylabel('Count rate per Minute')
plt.title('Upper 5cm')



# %%
