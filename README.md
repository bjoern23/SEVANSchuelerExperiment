# SEVANSchuelerExpriment
Dies ist ein Projekt für ein Schüler Experiment mit dem Detektor SEVAN. Es geht hierbei um die Datenauswertung eines kleinen Astroteilchenphysik Versuches. 
Bisher handelt es sich hierbei um einen kleinen Test umherauszufinden was mit dem Detektor möglich ist.
Eine Dokumentaion des Detektors kann [hier](http://www.crd.yerphi.am/files/SEVAN10_years.pdf) gefunden werden und die Datensätze aller Detektoren sind [hier](http://www.crd.yerphi.am/adei/) verfügbar.
## Voraussetzungen:
* Python3
* Benötigte Bibliotheken: [Matplotlib](https://matplotlib.org/), [Pandas](https://pandas.pydata.org/), [NuPy](https://numpy.org/install/) und OS
