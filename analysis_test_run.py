
import pandas as pd # Laden des Datenanalyse Frameworks
import numpy as np  #Laden von NumPy für erweiterte statistische Methoden
import matplotlib.pyplot as plt #Laden des Plotters
import os 

#Prüfe ob der Ordner plots exsistiert und wenn nicht soll dieser Ordner erstellt werden, um die Diagramme zu speichern
if not os.path.isdir("plots"):
    os.mkdir("plots")
    print("Ordner plots wird erstellt")


#Daten aus CSV Datei laden
berlinData = pd.read_csv("test/Data_BerlinV2.csv",index_col=0, parse_dates=["Date/Time"])
aragatsData = pd.read_csv("test/Data_AragatsV2.csv", index_col=0, parse_dates=["Date/Time"])
"""
The usage of the index_col and parse_dates parameters of the read_csv function to define the first (0th)
column as index of the resulting DataFrame and convert the dates in the column to Timestamp objects, respectively.
"""




#Kontrolle des daten Satzes, der ersten 8 Zeilen
print(berlinData.head(8))
print(aragatsData.head(8))
#Kombination beider Datensätze
combaindeData = pd.merge(berlinData, aragatsData,how="left", on="Date/Time")
"""
Als schlüssel wird Date/Time benutzt und
die Daten aus Aragats werden als neue Spalte links eingfügt
"""
#Umbenennung der Spalten zur besseren Handhabung
combaindeData = combaindeData.rename(
    columns={
         " SEVAN Berlin  Coincidence 101&111": "Berlin101&111",
         " SEVAN Aragats  Coincidence 101&111": "Aragats101&111",
         " SEVAN Berlin  Coincidence 101": "Berlin101",
         " SEVAN Aragats  Coincidence 101": "Aragats101",
         " SEVAN Berlin  Coincidence 111": "Berlin111",
         " SEVAN Aragats  Coincidence 111": "Aragats111",
    }
)

#Alle leeren Zellen und Zellen mit Leerzeichen werden in NaNs umgewandelt 
combaindeData = combaindeData.replace('', np.nan)
combaindeData = combaindeData.replace(' ', np.nan)

#Alle Zeilen mit NaN Zellen werden entfernt
combaindeData = combaindeData.dropna()

# Wenn möglich sollen alle Spalten ein numerischen Datentypen haben
combaindeData = combaindeData.apply(pd.to_numeric)

#Kontrolle der kobinierten daten Sätze, der ersten 8 Zeilen
print(combaindeData.head(8))

#Kontroll Plot der Muonen Rate beider Detektoren
fig, axs = plt.subplots(figsize=(12, 4))

combaindeData["Aragats101&111"].plot(ax=axs, label="Aragats", color='tab:orange')
combaindeData["Berlin101&111"].plot(ax=axs, label="Berlin", color='tab:blue')
axs.set_ylabel("Muon Rate")

fig.savefig("plots/testPlot.png")


dailyMean = combaindeData.groupby(combaindeData.index.weekday).agg([np.mean, np.std])
print(dailyMean["Berlin101&111"])

fig, axs = plt.subplots(figsize=(12, 4))
dailyMean["Aragats101&111"].plot(kind="bar", rot=0, ax=axs, y="mean", yerr="std", label="Aragats", color='tab:orange')
dailyMean["Berlin101&111"].plot(kind="bar", rot=0, ax=axs, y="mean", yerr="std", label="Berlin", color='tab:blue')
axs.set_ylabel("Muon Rate per Day")
axs.set_xlabel("Day")
fig.savefig("plots/PlotDailyMean.png")

#Brechnung des Flusses:
# Der Kanal des 111 muss auf ein Quadratmeter normiert werden 
flux = combaindeData
flux["Berlin111"] = flux["Berlin111"]/0.25
flux["Aragats111"] = flux["Aragats111"]/0.25
flux["Berlin101&111"] = flux["Berlin111"]+flux["Berlin101"]
flux["Aragats101&111"] = flux["Aragats101"]+flux["Aragats111"]
dailyFlux= flux.groupby(flux.index.weekday).agg([np.mean, np.std])

fig, axs = plt.subplots(figsize=(12, 4))
dailyFlux["Aragats101&111"].plot(kind="bar", rot=0, ax=axs, y="mean", yerr="std", label="Aragats", color='tab:orange')
dailyFlux["Berlin101&111"].plot(kind="bar", rot=0, ax=axs, y="mean", yerr="std", label="Berlin", color='tab:blue')
axs.set_ylabel("Muon Rate per Day per square meter")
axs.set_xlabel("Day")
fig.savefig("plots/PlotDailyFlux.png")
#Mittelwert aller Tage
fig, axs = plt.subplots(figsize=(12, 4))
FluxAragats = dailyFlux["Aragats101&111"]["mean"].agg([np.mean, np.std])
FluxBerlin = dailyFlux["Berlin101&111"]["mean"].agg([np.mean, np.std])
axs.errorbar([3200],[FluxAragats["mean"]],[FluxAragats["std"]],marker='o')
axs.errorbar([35], [FluxBerlin["mean"]],[FluxBerlin["std"]],marker='o')
axs.set_ylabel("Muon Rate per Day per square meter")
axs.set_xlabel("Atmospheric depth [m]")
fig.savefig("plots/PlotFluxMean.png")
#To-Do: Einfügen der Theorie Linie https://arxiv.org/pdf/astro-ph/0512125.pdf
print("Hallo world")