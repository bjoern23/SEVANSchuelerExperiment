# -*- coding: utf-8 -*-
# %% [markdown]
# # Bestimmung der Höhe einer Messstaion anhand des Myonenflusses

# %% [markdown]
# Importieren von pandas und pyplot
import os
import pandas as pd
import matplotlib.pyplot as plt

# %% [markdown]
# Daten aus CSV Datei laden
path = "Data/"
datasetenames = ["Aragats","Hamburg","Moussala","Prague"]
datasets = [pd.read_csv(path+name+".csv",index_col=0,parse_dates=["Date/Time"]) for name in datasetenames]

# %% [markdown]
# Daten ausgeben
print(datasets)

# %% [markdown]
# Spalten, zur besseren handhabe umbenennen
for i in range(len(datasets)):
    datasets[i] = datasets[i].rename(columns={" SEVAN "+datasetenames[i]+"  Coincidence 101&111": datasetenames[i]})

# %% [markdown]
# Daten vereinigen und darstellen
data =pd.concat(datasets)
data.plot()


# %% [markdown]
# Histogramm mit Mittelwert und Standradabweichung plotten und Werte abspeichern
means = []
stds = []
data.plot.hist(bins=100)
for name in datasetenames:
    mean = data[name].mean()
    std = data[name].std()
    plt.axvline(x=mean, color='black', linestyle='dashed', label='Mittelwert')
    plt.axvline(x=mean-std, color='grey', linestyle='dashed', label='Standartabweichung')
    plt.axvline(x=mean+std, color='grey', linestyle='dashed')
    means.append(mean)
    stds.append(std)
summeryData = pd.DataFrame({"Citys": datasetenames, "Means": means, "Stds": stds})

# %% [markdown]
# Filnaler Plot der Mittelwerte und Standradabweichungen
summeryData = summeryData.sort_values(by=["Means"])
summeryData.plot(kind ="scatter", x="Citys",y="Means", yerr=summeryData["Stds"] )




'''
- Teilchenarten und vgl Theorie und Experiment
- vgl zum Wetter
- vgl Standort, Höhenlage bestimmen

-Höhe

Datensätze 101&111

Aragats
Hamburg
Praque
Yerevan
Kosice
Zagreb
Moussala

'''

'''was ist eine Rate'''

'''Daten einlesen'''




'''Darstellen'''



'''auf die Höhe des Standortes schließen, (in der Mappe über Zusammnehänge schreiben'''

# %%
